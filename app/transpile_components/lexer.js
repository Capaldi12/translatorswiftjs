(function(trans) {

  /**
   * Это лексический анализатор.
   * @param {string} inputCode - строка с кодом программы на входном языке
   * @return {Array} - массив распознанных лексем
   */
  trans.TransProto.lexer = function (inputCode) {
    var lex = this.lexTable;
    /*for(var i in this.arrayOfSymbols){
      this.arrayOfSymbols[i] = [];
    }*/
    var result;
    var colomn=1,row=1;
    while(inputCode)
    { 
      for(var lexGroup in lex)//для каждой группы лексем
      {
        result=lex[lexGroup].regex.exec(inputCode);   
        if(result && result[0])
        {        
          if(!lex[lexGroup].skip)//если группу не пропускаем
          {
            
            if(!lex[lexGroup].list)//если константа или идентификатор или спэйс
            {
              console.log(this);
              var const_arr=this.arrayOfSymbols[lex[lexGroup].link];
              var const_ind=const_arr.indexOf(result[0]);//запишем индекс константы в массиве
              
              if(const_ind==-1){//если в массиве еще не было такой константы
                const_ind=const_arr.length;
                const_arr.push(result[0])//добавим константу в массив
              }
              this.lexArray.push([lex[lexGroup].link,const_arr[const_ind],row,colomn]);
              
              if(lex[lexGroup].link=="arrayOfSpace" )//если пробел
              {
                row++;
                colomn=result[0].length-1;
              }
              else
              colomn=colomn+result[0].length;
              inputCode = inputCode.replace(lex[lexGroup].regex, "");
            }
            else //если оператор или ключевое слово
            for(var nameList in lex[lexGroup].list)
            { 
              for(var subname in lex[lexGroup].list[nameList])
              {    
                var lexname=lex[lexGroup].list[nameList][subname];          
                if(lexname==result[0])//если найденный элемент присутствует в списке лексем текущей группы
                {
                  this.lexArray.push([lexname,row,colomn])//записать его в массив лексем
                  colomn=colomn+result[0].length;
                  inputCode = inputCode.replace(lex[lexGroup].regex, "");
                  break;
                } 
              }
            }
          }
          else{ //засчитать перенос строки
            if(result[0]=="\n"){
              row++;
              colomn=1;
            }
            inputCode = inputCode.replace(lex[lexGroup].regex, "");
          }
        } 
      }
    }
    return this.lexArray;
  }
})(window.trans || (window.trans = {}));
